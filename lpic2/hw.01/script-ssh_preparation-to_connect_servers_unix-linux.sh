#!/usr/bin/env bash
########################################################################
#Created by: A.G.
#Purpose: Exercise
#Date: 07/2/2021
#version: 1.0.0
########################################################################


main() {
	sudofunc
	createuser
	chkgra
	dissrv
	instssh
	configipstatic
}

####################### config ip static ##########################
configipstatic() {
	clear
	printf "%s Your IP address is. \n"
	ip a | grep inet'\s' #####command for get IP address.
	printf "%s\nTo continue Enter:\n\n\n"
	read 
	clear
	printf "%s\nEnter 1 for edit ip with nmtui if not ENTER to continue.\n"
	read entered	
if [[ "1" == "$entered" ]];then
	clear
	printf "%s\nYou choose 1 for nmtui.\nEnter to continue."
	nmtui #This command for changing ip static in graffiti.
	clear
else
	printf "%s\nYou choice to continue.\n"
	sleep 2
	clear
fi
}
####################### Create user name for remote server ##########################

createuser() {
#create user
opso=($(grep '^ID=' /etc/os-release | grep  [a-z] | sed 's/"//g' | sed 's/ID=//g'))
#opso1=($(cat /etc/*-release | grep -E "^ID=" | cut -d"=" -f2 | tr -d \"))
#opso2=($(cat /etc/*-release | grep -E "^ID=" | cut -d"=" -f2 | sed 's/"//g'))
clear
printf "%s\nCreating  user:\n\nCreateing user for Debian or Redhat with permission sudo.\n\n"
while true; do
	read -p "To continue insert y/n:" yn
	case $yn in
	[Yy]*) if [[ "$opso" == "debian" ]];then
			printf "%s Create user for server\nEnter user name: "
			read username
			printf "%s\nWrite name for comment: "
			read comment
			useradd -m -G adm,sudo -c "$comment" $username
			printf "%s\nEnter password for user $username:"
		        passwd $username
			chkenv
			sleep 2
			clear
		elif [[ "$opso" == "centos" ]];then
			printf "%s Create user for server\nEnter user name: "
			read username
			printf "%s\nWrite name for comment: "
			read comment
			useradd -m -G adm,wheel -c "$comment" $username
			printf "%s\nEnter password for user $username:\n"
		        passwd $username
			chkenv
			sleep 2;clear
		fi;break;;
	[Nn]*) printf "%s\nContinueing without created user.\n\n\n";sleep 2;clear;break;;
	*) printf "%s\nPlease answer yes or no.";;
	esac
done
}
####################### checking if user created in env /bin/bash ##########################

# check the user if created write > under /bin/bash
chkenv () {
local createbash=($(cat /etc/passwd | tail -1 | grep /bash))
if [[ "$createbash" == "" ]];then
	usermod -s /bin/bash $username
fi
}
####################### check terminal graphiti ##########################

chkgra() {
#check graphical terminal
	clear
	local resultgra=($(systemctl get-default))
	printf "%sThe graphiti of trminal is $resultgra\n\nFor swich to multi-user.target ENTER Y.\nTo stay in $resultgra ENTER N.\n"
while true; do
	read -p "Do you wish to swich the graphical?" yn
	case $yn in
		[Yy]*) systemctl set-default multi-user.target;break;;
		[Nn]*) printf "%s\nYour choice is $ch to stay in $resultgra.\n\n";sleep 2;break;;
		*) printf "%s\nPlease answer yes or no.";;
		esac
done
}
####################### access sudo ##########################

#if i run in sudo
sudofunc() {
clear
local username1=($(cat /etc/group | grep wheel | grep -o $USER))
local username2=($(cat /etc/group | grep sudo | grep -o $USER))

if [[ "$username1" == "$USER" ]]; then
	printf "%s\n You have permissions for SUDO.\n"
	sleep 2
elif [[ "$usename2" == "$USER" ]]; then
	printf "%s\n You have permissions for SUDO.\n"
	sleep 2
elif [[ "$EUID" == "0" ]];then
	printf "%s\n You have permissions this user is root.\n"
	sleep 2
else
	printf "%s\n You do not have permissions for SUDO.\n"
	exit 1
fi
}
####################### install SSH ##########################
instssh() {
	clear	
printf "%s\nInstalling SSH:\nFor deabian(family) write apt.\nFor Redhat(family) write yum.\n"

sshexi=($(systemctl -t service | grep ssh.service))
if [[ "$sshexi" == "" ]];then
        printf "%s\nInstall SSH.\n\n"
	while true; do
		read -p "Write apt or yum: " typ
		case $typ in
        	[yum]*) yum install openssh-server;break;;
		[apt]*) apt install openssh-server;break;;
		[no]*) break;;
		*) printf "%s\nWrite apt or yum to install (for exit write no): ";;
	esac
done
else
        printf "%s\nSSH is installed and checking... \n$sshexi\n\n"
fi
}
#######################  To disable services unwanted  ##########################

#To disable services unwanted
dissrv() {
clear
printf "%s\nTo disable unwanted services:\n\nTo disable services,\n             > ufm apparmor insert 1 and ENTER. \nTo disable services,\n             >ufm lightdm apparmor avahi-daemon blk-availability colord alsa-restore alsa-state insert 2 and ENTER.\n\nTo contenue without diasable services Enter to Countinue.\nYour choice is: "
read inssrv
if [[ "$inssrv" == "1" ]];then
        printf "%s\nDisable sevices.\n\n"
        stemctl --now  disable ufm apparmor
elif [[ "$inssrv" == "2" ]];then
        printf "%s\nDisable sevices.\n\n"
        stemctl --now  disable ufm lightdm apparmor avahi-daemon blk-availability colord alsa-restore alsa-state
else
        printf "%s\nYou choice is to continue without changeing.\n\n"
fi
}
main $@
