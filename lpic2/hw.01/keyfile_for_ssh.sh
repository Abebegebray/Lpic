#!/user/bin/env bash
########################################################################
#Created by: A.G.
#Purpose: Exercise
#Date: 07/2/2021
#version: 1.0.0
########################################################################

#how to save password in remote server
clear
printf "%sNotes:\n\n    The key files is saved in current folder in names.\n\n1. Only with the name you gave him.\n2. With the name you gave him and '.pub'.\n\n"
printf "%s\nCreate key for ssh: \nHow you name your file key.\nWrite the file name: "
        read filename
        printf "$filename\n\n\n" | ssh-keygen
        sleep 1
        clear
        printf "%s\nNow enter details of the server that are going to take over.\n\n"
        printf "%s\nEnter user name of the server: "
        read rusername
        printf "%s\nEnter IP of server: "
        read rip
        sleep 0.5
if ssh -t $rusername@$rip '[ -d ~/.ssh ]' ; then
        clear
        printf "%s\nMoving file key '.pub.....'\n\n"
        scp $filename.pub $rusername@$rip:~/.ssh
        ssh -o IdentitiesOnly=yes $rusername@$rip "cp -p ~/.ssh/$filename.pub ~/.ssh/authorized_keys"
else
        clear
        printf "%s\nCreate folder '.ssh' in the remote server.\n\n And moving file key '.pub'\n"
        ssh -o IdentitiesOnly=yes $rusername@$rip "mkdir -p ~/.ssh"
        scp $filename.pub $rusername@$rip:~/.ssh
        ssh -o IdentitiesOnly=yes $rusername@$rip "cp -p ~/.ssh/$filename.pub ~/.ssh/authorized_keys"
fi
	clear
	printf "%s\nRun the command inside '#' your terminal.\n"
        printf "%s\n	You will log in to the server $rip without ENTERING a password.\n\n"

        l="###################################################################"

        printf "%s\n$l\n# ssh -i $filename -o IdentitiesOnly=yes $rusername@$rip\n$l\n\n\n"
