# Home work
- create a script that installs and configures web server.
	- install should be done remotely, by using ssh.
	- install should be done in consideration to Linux OS.(RedHat/Debian/Arch families)
	- there should be error handling in case something will go wrong which will result in script stop.
	- the use of script should be provided to root only.
	- use of sudo is forbidden.
	- saving the password in script is forbidden.
	- one could save temorarily the password as system variable and delete it once the script ends.
