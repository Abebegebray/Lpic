answer ch 1/2 plic1

1.
[root@student Practice]# pidof bash
2754 2448 972

2.

[root@student Practice]# echo $$
2754

3.[root@student Practice]# whoami
root

4.
ps -U root
ps -U root -u root
or
ps aux | grep root

5.
[root@student Practice]# echo $$
2754

6.
root@student Practice]# echo $PPID
2746
[root@student Practice]# echo $$ $PPID
2754 2746

7.
[root@student ~]# sleep 3342 &
[1] 2737
[root@student ~]# sleep 3342 &
[2] 2738

8.
[root@student ~]# pgrep sleep
2735
2737
2738
[root@student ~]# ps -C  sleep
    PID TTY          TIME CMD
   2737 pts/0    00:00:00 sleep
   2738 pts/0    00:00:00 sleep
   2748 ?        00:00:00 sleep
9.
[root@student ~]# top -c -p 2737,2738

top - 18:13:58 up 8 min,  1 user,  load average: 0.09, 0.44, 0.31
Tasks:   2 total,   0 running,   2 sleeping,   0 stopped,   0 zombie
%Cpu(s):  2.0 us,  0.3 sy,  0.0 ni, 97.0 id,  0.0 wa,  0.3 hi,  0.3 si,  0.0 st
MiB Mem :   3939.0 total,   1951.0 free,   1207.1 used,    780.9 buff/cache
MiB Swap:   3280.0 total,   3280.0 free,      0.0 used.   2484.8 avail Mem 

    PID USER      PR  NI    VIRT    RES    SHR S  %CPU  %MEM     TIME+ COMMAND  
   2737 root      20   0    7280    712    648 S   0.0   0.0   0:00.00 sleep 3+ 
   2738 root      20   0    7280    728    664 S   0.0   0.0   0:00.00 sleep 3+ 

10.
[root@student ~]# kill 2737

11.
[root@student ~]# killall sleep
[1]-  Terminated              sleep 3342
[2]+  Terminated              sleep 3342
